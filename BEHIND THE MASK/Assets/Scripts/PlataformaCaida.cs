﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaCaida : MonoBehaviour
{
    public Rigidbody Rb;
    public Collider c;

    void Start()
    {
        Rb = GetComponent<Rigidbody>();
        Rb.constraints = RigidbodyConstraints.FreezePositionY|RigidbodyConstraints.FreezeRotationY;
        c = GetComponent<Collider>();
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rb.constraints = RigidbodyConstraints.None;
            c.isTrigger = true;
        }

    }

    
}