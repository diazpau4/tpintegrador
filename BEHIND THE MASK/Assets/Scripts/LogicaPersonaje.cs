﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 
using UnityEngine;

public class LogicaPersonaje : MonoBehaviour
{

    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;

    public Animator anim;
    public float x, y;

    public Rigidbody rb;
    public float fuerzaSalto = 5f;
    public bool puedoSaltar;

    public int itemsCollected;
    public Text textAmountCollected;
    public GameObject star;
    public GameObject puerta;

    public bool estoyAtacando;
    public bool avanzoSolo;
    public float impulsoDeGolpe = 10f;


    void Start()
    {
        puedoSaltar = false;
        anim = GetComponent<Animator>();
        SetTexts();
    }

    private void SetTexts()
    {
        textAmountCollected.text = "CARTAS: " + itemsCollected.ToString() + "/3";
    }

    void FixedUpdate()
    {
        if (!estoyAtacando)
        {
            transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
            transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
        }
        
        if (itemsCollected == 3)
        {
            star.SetActive(true);
        }

        if (avanzoSolo)
        {
            rb.velocity = transform.forward * impulsoDeGolpe;
        }
    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Return) && puedoSaltar && !estoyAtacando)
        {
            anim.SetTrigger("golpeo");
            estoyAtacando = true;
        }

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (puedoSaltar)
        { 
            if (!estoyAtacando)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    anim.SetBool("Salto", true);
                    rb.AddForce(new Vector3(0, fuerzaSalto, 0), ForceMode.Impulse);

                }
            }
            anim.SetBool("TocoSuelo", true);
        } else
        {
         estoyCayendo();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            itemsCollected++;
            SetTexts();
            other.gameObject.SetActive(false);
        }
        if (other.tag == "Stary")
        {
            AbrePuerta();
        }

        if (other.gameObject.CompareTag("C1"))
        {
            GestorDeAudio.instancia.ReproducirSonido("C1");
        }

        if (other.gameObject.CompareTag("C2"))
        {
            GestorDeAudio.instancia.ReproducirSonido("C2");
        }

        if (other.gameObject.CompareTag("C3"))
        {
            GestorDeAudio.instancia.ReproducirSonido("C3");
        }

        if (other.gameObject.CompareTag("C4"))
        {
            GestorDeAudio.instancia.ReproducirSonido("C4");
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("C1"))
        {
            GestorDeAudio.instancia.PausarSonido("C1");

        }

        if (other.gameObject.CompareTag("C2"))
        {
            GestorDeAudio.instancia.PausarSonido("C2");

        }

        if (other.gameObject.CompareTag("C3"))
        {
            GestorDeAudio.instancia.PausarSonido("C3");

        }

        if (other.gameObject.CompareTag("C4"))
        {
            GestorDeAudio.instancia.PausarSonido("C4");

        }
    }


    public void estoyCayendo()
    {
      anim.SetBool("TocoSuelo", false);
      anim.SetBool("Salto", false);
    }

    private void AbrePuerta()
    {
        puerta.SetActive(true);
    }

    public void DejeDeGolpear()
    {
        estoyAtacando = false;
        avanzoSolo = false;
    }
    public void AvanzoSolo()
    {
        avanzoSolo = true;
    }

    public void DejoDeAvanzar()
    {
        avanzoSolo = false;
    }
   
}
