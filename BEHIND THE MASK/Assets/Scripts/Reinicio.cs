﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reinicio : MonoBehaviour
{
    public GameObject puntoDeSalida;
    public GameObject Cubo;
    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("ReinicioCube")==true) 
       {
            transform.position = new Vector3(puntoDeSalida.transform.position.x, puntoDeSalida.transform.position.y, puntoDeSalida.transform.position.z);
            Cubo.SetActive(false);
       } 

       
    }
}