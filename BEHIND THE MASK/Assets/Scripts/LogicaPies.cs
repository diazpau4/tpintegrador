﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPies : MonoBehaviour
{
    public LogicaPersonaje Lg; 


    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Untagged"))
        Lg.puedoSaltar = true;

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged"))
        Lg.puedoSaltar = false;
    }

}
