﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataforma1 : MonoBehaviour
{
    public Rigidbody rb;
    public Transform[] pos;
    public float speed;

    private int posactual = 0;
    private int possig = 1;

    void Start()
    {
        
    }

    
    void Update()
    {
        MovePlatform();
    }

    void MovePlatform()
    {
        rb.MovePosition(Vector3.MoveTowards(rb.position, pos[possig].position, speed * Time.deltaTime));

        if (Vector3.Distance(rb.position, pos[possig].position) <= 0)
        {
            posactual = possig;
            possig++;

            if(possig > pos.Length - 1)
            {
                possig = 0;
            }
            
        }
    }
}
