﻿using UnityEngine;

public class Enemigo : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;


    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Personaje");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
       if (collision.gameObject.CompareTag("Player"))
        {
           recibirDaño();
        }
    }

    public void recibirDaño()
    {
        hp = hp - 100;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }
}
