﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaN1 : MonoBehaviour
{

    public GameObject Star;
    public GameObject puert;

    private void AbrePuerta()
    {
        puert.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Starr")
        {
            AbrePuerta();
        }
    }
}